<?php

use App\TypeFormatter;

$logo = 'Product List';

require_once './app/Views/layouts/header.php';

?>
<html lang="en">
<body class="body">
<form action="/delete" method="post">
    <input type="hidden" name="_method" value="DELETE" />
    <div class="header">
        <div class="logo">
            <a href="/" class="logo">Product list</a>
        </div>
        <div class="header-right">
            <a class="btn btn-outline-secondary" href="/addProduct" role="button">Add Product</a>
            <!-- double validation in html, do check if user wants to delete these item, second goes in backend -->
            <input type="submit" value="Delete" name="delete" onclick="return confirm('Are you sure?')"
                   class="btn btn-outline-secondary ml-3">
        </div>
    </div>
    <hr>
    <br>
    <div class="container-fluid">
        <div class="row">
            <?php if (!empty($products)) : ?>
                <?php foreach ($products as $product) : ?>
                    <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6 col-12 mb-5">
                        <div class="card bg-light mb-3 text-center" style="max-width: 18rem;">
                            <input type="checkbox" id="checkbox[]" name="checkbox[]"
                                   value="<?php echo htmlspecialchars($product->getId()); ?>">
                            <div class="card-header"><?php echo htmlspecialchars($product->getSku()); ?></div>
                            <div class="card-body">
                                <h5 class="card-title"><?php echo htmlspecialchars($product->getName()); ?></h5>
                                <p class="card-text"><?php echo htmlspecialchars(TypeFormatter::moneyFormat($product->getPrice())); ?></p>
                                <p class="card-text"><?php echo htmlspecialchars(ucfirst($product->getType())) . ': '
                                        . htmlspecialchars($product->getDescription()) . ' '
                                        . htmlspecialchars(TypeFormatter::descriptionFormat($product->getType())); ?></p>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            <?php endif; ?>
        </div>
    </div>
    </div>
</form>
</body>
<footer>
    <?php require_once './app/Views/layouts/footer.php'; ?>
</footer>
</html>