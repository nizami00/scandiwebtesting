function validateForm() {
    const form = 'productForm'
    const option = document.forms[form]["switch"].value;


    if (!validateMainInput(document.forms[form]["sku"].value)) {
        alert("Please enter proper sku")
        return false
    }

    if (!validateMainInput(document.forms[form]["name"].value)) {
        alert("Please enter proper name")
        return false
    }

    if (!validateNumber(document.forms[form]["price"].value)) {
        alert("Please enter valid price in numbers")
        return false
    }


    switch (option) {
        case 'size' :
            if (!validateNumber(document.forms[form]['size'].value)) {
                alert("Please enter valid numbers for size")
                return false;
            }
            return true;
        case 'weight' :
            if (!validateNumber(document.forms[form]['weight'].value)) {
                alert("Please enter valid numbers for weight")
                return false
            }
            return true;
        case 'dimensions' :
            if (!validateNumber(document.forms[form]['length'].value) ||
                !validateNumber(document.forms[form]['width'].value) ||
                !validateNumber(document.forms[form]['height'].value)) {
                alert("Please enter valid numbers for length, width and height")
                return false
            }
            return true;
        default :
            alert("Please fill all forms correctly")
            return false;
    }

}

function validateMainInput(input) {
    return input.toString().trim() !== '' && !/<[a-z/][\s\S]*>/i.test(input);
}

function validateNumber(number) {
    return /^[0-9]*[.]?[0-9]*$/.test(number) && number > 0
}

$("#switch").on("change", function () {
    $(".option").hide();
    const a = $("#" + $(this).val()).show();
    console.log(a);
})