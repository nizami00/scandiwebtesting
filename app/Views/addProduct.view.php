<?php

$logo = 'Add Product';

require_once './app/Views/layouts/header.php';
?>
<html lang="en">
<body>
<!-- funeral meme was a good one D: -->
<?php if (isset($error)) : ?>
    <script>alert('Oops, there is something wrong with an input')</script>
<?php endif ;?>
<form id="contact_form" name="productForm" action="/storeProduct" method="post" onsubmit="return validateForm()">
    <div class="header">
        <div class="logo">
            <a href="/" class="logo">Product list</a>
        </div>
        <div class="header-right">
            <input type="submit" name="save" value="Save" onclick="return confirm('Are you sure?')"
                   class="btn btn-outline-secondary">
            <a class="btn btn-outline-secondary ml-3" href="/" role="button"
               onclick="return confirm('Are you sure?')">Cancel</a>
        </div>
    </div>
    </br>
    <div id="container">
        <h1>Add Product</h1>
        <label for="sku">SKU</label>
        <input type="text" id="sku" name="sku" placeholder="Add SKU number" required>
        <label for="name">Name</label>
        <input type="text" id="name" name="name" placeholder="Add name of the product" required>
        <label for="price">Price</label>
        <input type="number" min="0" step="0.01" id="price" name="price"
               placeholder="Add price of the product" required>
        <div class="form-group row">
            <label for="switch">Choose Description</label>
            <select class="form-control" name="option" id="switch" required>
                <option selected disabled value=''></option>
                <option name="size" value="size">Disc</option>
                <option name="weight" value="weight">Book</option>
                <option name="dimensions" value="dimensions">Furniture</option>
            </select>
        </div>
        <div class="form-group option" id="size" style="display: none;">
            <label for="size">Size (MB)</label>
            <input type="number" min="1" name="descriptions[]" class="form-control require-if-active" id="size"
                   placeholder="Please choose size of the disc (MB)">
            <small>Please, provide size</small>
        </div>
        </br>
        <div class="form-group option" id="weight" style="display: none;">
            <label for="weight">Weight (KG)</label>
            <input type="number" step="0.001" name="descriptions[]" class="form-control require-if-active" id="weight"
                   placeholder="Please choose weight of the book (KG)">
        </div>
        </br>
        <div class="form-group option" id="dimensions" style="display: none;">
            <label for="height">Height (CM)</label>
            <input type="number" min="0" name="descriptions[]" class="form-control require-if-active" id="height"
                   placeholder="Please choose height of the furniture">
            <label for="width">Width (CM)</label>
            <input type="number" min="0" name="descriptions[]" class="form-control require-if-active" id="width"
                   placeholder="Please choose width of the furniture">
            <label for="length">Length (CM)</label>
            <input type="number" min="0" name="descriptions[]" class="form-control require-if-active" id="length"
                   placeholder="Please choose length of the furniture">
            <small>Please, provide height, width and length of the product</small>
        </div>
    </div>
</form>

<script type="text/javascript" src="/app/Views/addProduct.js"></script>
</body>
<footer>
    <?php require_once './app/Views/layouts/footer.php'; ?>
</footer>
</html>