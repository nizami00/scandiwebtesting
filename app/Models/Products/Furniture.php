<?php

namespace App\Models\Products;

class Furniture extends Product implements ProductInterface
{
    private string $dimensions;

    public function __construct(
        string $id,
        string $sku,
        string $name,
        int $price,
        string $type,
        string $dimensions
    )
    {
        parent::__construct($id, $sku, $name, $price, $type);
        $this->dimensions = $dimensions;
    }

    public function getDescription(): string
    {
        return $this->dimensions;
    }


    public static function initialize(array $product): Furniture
    {
        return new self(
            $product['id'],
            $product['SKU'],
            $product['name'],
            (int)$product['price'],
            $product['type'],
            $product['description']
        );
    }
}