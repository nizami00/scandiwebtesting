<?php

namespace App\Models\Products;

interface ProductInterface
{
    public static function initialize(array $product): Product;

}