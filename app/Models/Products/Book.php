<?php

namespace App\Models\Products;

class Book extends Product implements ProductInterface
{
    private float $weight;

    public function __construct(
        string $id,
        string $sku,
        string $name,
        int $price,
        string $type,
        float $weight)
    {
        parent::__construct($id, $sku, $name, $price, $type);
        $this->weight = $weight;
    }

    public function getDescription(): float
    {
        return $this->weight;
    }

    public static function initialize(array $product): Book
    {
        return new self(
            $product['id'],
            $product['SKU'],
            $product['name'],
            (int)$product['price'],
            $product['type'],
            (float)$product['description'],
        );
    }
}