<?php

namespace App\Models\Products;


class Product
{
    private string $id;
    private string $sku;
    private string $name;
    private int $price;
    protected string $type;

    public function __construct(
        string $id,
        string $sku,
        string $name,
        int $price,
        string $type
    )
    {
        $this->id = $id;
        $this->sku = $sku;
        $this->name = $name;
        $this->price = $price;
        $this->type = $type;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getSku(): string
    {
        return $this->sku;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function getPrice(): int
    {

        return $this->price;
    }

}