<?php

namespace App\Models\Products;

class Disc extends Product implements ProductInterface
{
    private float $size;

    public function __construct(
        string $id,
        string $sku,
        string $name,
        int $price,
        string $type,
        float $size)
    {
        parent::__construct($id, $sku, $name, $price, $type);
        $this->size = $size;
    }

    public function getDescription(): int
    {
        return $this->size;
    }

    public static function initialize(array $product): Disc
    {
        return new self(
            $product['id'],
            $product['SKU'],
            $product['name'],
            (int)$product['price'],
            $product['type'],
            (float)$product['description'],
        );
    }
}