<?php

namespace App\Models;

use App\Models\Products\{Book, Disc, Furniture};
use App\Models\Products\Product;

class ProductsCollection
{
    private array $products = [];

    public function __construct(array $productsQuery)
    {
        $this->load($productsQuery);
    }

    private function load(array $productsQuery): void
    {
        //use collection array to store all products
        foreach ($productsQuery as $product) {
            switch ($product['type']) {
                case 'weight' :
                    $this->products[] = Book::initialize($product);
                    break;
                case 'size' :
                    $this->products[] = Disc::initialize($product);
                    break;
                case 'dimensions':
                    $this->products[] = Furniture::initialize($product);
                    break;
                default:
                    $this->products[] = null;
            }
        }

    }

    public function getAllProducts(): array
    {
        return $this->products;
    }
}