<?php

namespace App\Services;

class ValidationService
{
    //check if input is valid
    public function checkInput(string $input): bool
    {
        return trim($input) !== '';
    }

    //if user goes around html validation, test once again for number
    public function validateNumbers(string $number): bool
    {
        if (is_numeric($number) && $number > 0) {
            return true;
        }
        return false;
    }

    public function isHTML(array $postForm): bool
    {
        foreach ($postForm as $input) {
            if (!is_array($input)) {
                if ($input !== strip_tags($input)) {
                    return false;
                }
            }
        }

        return true;
    }


    public function checkForm(array $postForm): bool
    {
        //validate input
        if (
            !$this->checkInput($postForm['sku']) ||
            !$this->checkInput($postForm['name']) ||
            !$this->checkInput($postForm['option']) ||
            !$this->validateNumbers($postForm['price']) ||
            !$postForm['descriptions']) {
            return false;
        }

        if (!$this->checkDescription($postForm)) {
            return false;
        }

        return true;
    }

    public function checkDescription(array $postForm): bool
    {
        $descriptions = ['size', 'weight', 'dimensions'];

        if (!$postForm['option'] || !in_array($postForm['option'], $descriptions)) {
            return false;
        }

        $description = array_filter($postForm['descriptions'], function ($description) {
            return trim($description) > 0 && is_numeric($description);
        });

        if (!$description) {
            return false;
        }

        return true;
    }


}