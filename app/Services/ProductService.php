<?php

namespace App\Services;

use App\ProductRepository;

class ProductService
{

    private ProductRepository $productRepository;

    public function __construct()
    {
        $this->productRepository = new ProductRepository();
    }

    public function saveProduct(): void
    {
        $this->productRepository->storeProduct();
    }

    public function loadProducts(): array
    {
        return query()
            ->select(
                'products.id',
                'products.SKU',
                'products.name',
                'products.price',
                'product_properties.type',
                'product_properties.description'
            )
            ->from('products')
            ->from('product_properties')
            ->where('products.id = product_properties.id')
            ->execute()
            ->fetchAllAssociative();
    }

    public function deleteProduct(array $products): void
    {
        $this->productRepository->deleteProduct($products);
    }
}