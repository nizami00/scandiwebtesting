<?php

namespace App\Controllers;

use App\Models\ProductsCollection;
use App\Services\ProductService;
use App\Services\ValidationService;
use Doctrine\DBAL\Exception\InvalidArgumentException;


class ProductController
{

    public function showProducts()
    {
        //load products to main page if they exist
        if ($productsQuery = (new ProductService())->loadProducts()) {
            $products = (new ProductsCollection($productsQuery))->getAllProducts();
        }


        return require_once __DIR__ . '/../Views/main.view.php';
    }

    public function showAddProductForm()
    {
        //redirect to the product form
        return require_once __DIR__ . '/../Views/addProduct.view.php';
    }

    public function storeProduct()
    {

        $validationService = new ValidationService();
        /*
        if form is not valid return back
        to main page without starting a service
        */
        if (!$validationService->checkForm($_POST) || !$validationService->isHTML($_POST)) {
            //if validation sees something wrong return error to the add product page

            $error = true;

            return require_once __DIR__ . '/../Views/addProduct.view.php';
        }

        (new ProductService())->saveProduct();

        return header('Location: /');
    }

    public function deleteProduct()
    {
        //if user presses button without selecting
        if (!isset($_POST['checkbox'])) {
            return header('Location: /');
        }

        (new ProductService())->deleteProduct($_POST['checkbox']);

        return header('Location: /');

    }
}