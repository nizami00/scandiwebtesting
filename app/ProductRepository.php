<?php

namespace App;

use App\Services\ValidationService;

class ProductRepository
{

    public function storeProduct(): void
    {

        //create id to work with two tables
        $id = uniqid();

        //insert data in products table
        $this->saveProduct($id);

        //insert data in product description table
        $this->saveProductDescription($id);

    }

    private function saveProduct(string $id): void
    {
        query()
            ->insert('products')
            ->values(
                [
                    'id' => '?',
                    'SKU' => '?',
                    'name' => '?',
                    'price' => '?',
                ]
            )
            ->setParameters(
                [0 => $id,
                    1 => $_POST['sku'],
                    2 => $_POST['name'],
                    3 => ($_POST['price'] * 100)
                ])
            ->execute();
    }

    private function saveProductDescription(string $id): void
    {
        $descriptionsData = array_filter($_POST['descriptions'], function ($description) {
            return strlen(trim($description)) > 0;
        });

        $description = implode('x', $descriptionsData);


        //add properties for each product in properties table
        query()
            ->insert('product_properties')
            ->values(
                [
                    'id' => '?',
                    'type' => '?',
                    'description' => '?',
                ]
            )
            ->setParameters(

    [
                    0 => $id,
                    1 => $_POST['option'],
                    2 => $description
                ])
            ->execute();
    }

    public function deleteProduct(array $products): void
    {
        //delete products which were chosen by user
        foreach ($products as $product) {
            query()
                ->delete('products')
                ->where('id = :id')
                ->setParameter('id', $product)
                ->execute();

            query()
                ->delete('product_properties')
                ->where('id = :id')
                ->setParameter('id', $product)
                ->execute();
        }
    }
}