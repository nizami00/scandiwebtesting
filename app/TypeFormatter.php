<?php

namespace App;

use NumberFormatter;

class TypeFormatter
{

    public static function descriptionFormat(string $type): string
    {
        switch ($type) {
            case 'size' :
                return 'MB';
            case 'weight' :
                return 'KG';
        }

        return '';
    }

    public static function moneyFormat(int $price): string
    {
        $fmt = new NumberFormatter('en_US', NumberFormatter::CURRENCY);
        return $fmt->formatCurrency($price / 100, "USD");
    }


}